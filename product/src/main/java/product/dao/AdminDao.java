package product.dao;

import java.sql.*;
import product.com.Admin;

public class AdminDao {
	public int signup(Admin ad) throws ClassNotFoundException {
		int record = 0;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");

			PreparedStatement ps = con.prepareStatement("insert into user values(?,?,?,?)");
			ps.setString(1,ad.getAdmin_username());
			ps.setString(2,ad.getAdmin_email());
			ps.setString(3,ad.getAdmin_password());
			ps.setInt(4,2);
			record = ps.executeUpdate();
			ps.close();
			return record;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return record;
	}

	public Admin login(String username,String password) throws ClassNotFoundException {
		Admin admin=null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
			PreparedStatement ps = con.prepareStatement("select * from user where username=? and password=? and role_id=2");
			 ps.setString(1, username);
			 ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			
			 if (rs.next()==true){
				 admin = new Admin(rs.getString(1),rs.getString(3));
				 System.out.println("Valid user, Login Successful!");
			} else if(rs.next()==false){
				System.out.println("Invalid User, kindly recheck your username or password.");
				
				}
			rs.close();
			ps.close();
			return admin;
			} catch (SQLException e){
				e.printStackTrace();
				}
		return admin;
		}
}
