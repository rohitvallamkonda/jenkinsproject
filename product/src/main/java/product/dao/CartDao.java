package product.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import product.com.Cart;

public class CartDao {
	public List<Cart> getAllCart() {
		List<Cart> cart = new LinkedList<>();
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
			Statement st = con.createStatement();
			String q = "select * from cart";
			ResultSet rs = st.executeQuery(q);

			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String desc = rs.getString(3);
				int quantity = rs.getInt(4);
				int price = rs.getInt(5);
				String img = rs.getString(6);
				cart.add(new Cart(id, name, desc, quantity, price, img));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cart;
	}

	public int insertcart(Cart c) {
		int record = 0;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
			String w = "update ecommerce set product_quantity = product_quantity-1 where product_id = ?";
			PreparedStatement p = con.prepareStatement(w);
			p.setInt(1, c.getId());
			int a = p.executeUpdate();

			String q = "insert into cart values (?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(q);

			ps.setInt(1, c.getId());
			ps.setString(2, c.getName());
			ps.setString(3, c.getDesc());
			ps.setInt(4, c.getQuantity());
			ps.setInt(5, c.getPrice());
			ps.setString(6, c.getPimage());
			record = ps.executeUpdate();

			return record;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return record;
	}

	public int delete(int id) {
		int delete = 0;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
			String e = "update ecommerce set product_quantity = product_quantity+1 where product_id = ?";
			PreparedStatement p = con.prepareStatement(e);
			p.setInt(1, id);
			p.executeUpdate();

			PreparedStatement ps = con.prepareStatement("delete from cart where id = ?");
			ps.setInt(1, id);
			delete = ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return delete;
	}
}
