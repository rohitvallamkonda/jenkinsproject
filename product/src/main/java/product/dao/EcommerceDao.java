package product.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import product.com.Ecommerce;

public class EcommerceDao {
	public List<Ecommerce> getEcommerce() {
		List<Ecommerce> prod = new ArrayList<>();
			try {
				Class.forName("org.mariadb.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
				Statement st = con. createStatement();
				String q = "select * from ecommerce";
				ResultSet rs = st.executeQuery(q);
				
	
				while(rs.next()) {
					String name = rs.getString("product_name");
					int id = rs.getInt("product_id");
					String desc = rs.getString("product_desc");
					int qty = rs.getInt("product_quantity");
					int price = rs.getInt("product_price");
					//String image = rs.getString("image");
					prod.add(new Ecommerce(name,id,desc,qty,price));
					}
				rs.close();
				st.close();
				}catch(Exception e1) {
					e1.printStackTrace();
					}
			return prod;
	}
	 public int insert(Ecommerce eco) throws ClassNotFoundException {
		 int record = 0;
		 	try {
		 		Class.forName("org.mariadb.jdbc.Driver");
		 		Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
		 		PreparedStatement ps = con.prepareStatement("insert into ecommerce values(?,?,?,?,?,?)");
		 		ps.setString(1, eco.getProduct_name());
		 		ps.setInt(2, eco.getProduct_id());
		 		ps.setString(3, eco.getProduct_desc());
		 		ps.setInt(4, eco.getProduct_quantity());
		 		ps.setInt(5, eco.getProduct_price());
		 		ps.setString(6,eco.getImage());
		 		record=ps.executeUpdate();
		 		ps.close();
		 		return record;
		 		} catch (SQLException e) {
		 			e.printStackTrace();
		 			}
		 	return record;
		 }
	 public int update(String product_name,int product_id,String product_desc,int product_price,int product_quantity) throws ClassNotFoundException {
		 int update = 0;
		 try {
			 Class.forName("org.mariadb.jdbc.Driver");
		 		Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
		 		PreparedStatement ps = con.prepareStatement("update ecommerce set product_name=?,product_desc=?,product_quantity=?,product_price=? where product_id=?");
		 		ps.setString(1, product_name);
		 		ps.setString(2, product_desc);
		 		ps.setInt(3, product_quantity);
		 		ps.setInt(4, product_price);
		 		ps.setInt(5, product_id);
		 		update=ps.executeUpdate();
		 		ps.close();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 return update;
	 }
	 public int delete(int id) throws ClassNotFoundException {
		 int delete = 0;
		 try {
			 Class.forName("org.mariadb.jdbc.Driver");
		 		Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/prod", "root", "admin");
		 		PreparedStatement ps = con.prepareStatement("delete from ecommerce where product_id=?");
		 		ps.setInt(1, id);
		 		delete=ps.executeUpdate();
		 		ps.close();
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
		 return delete;
	 }
}