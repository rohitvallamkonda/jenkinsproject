package product.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import product.com.Ecommerce;
import product.dao.EcommerceDao;

/**
 * Servlet implementation class DoposT
 */
public class DoposT extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoposT() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/form");
		
		EcommerceDao dao =new EcommerceDao();
		PrintWriter p =response.getWriter();
		
		String name=request.getParameter("product_name");
		int id =Integer.parseInt (request.getParameter("product_id"));
		String desc=request.getParameter("product_desc");
		int qty=Integer.parseInt(request.getParameter("product_quantity"));
		int price=Integer.parseInt(request.getParameter("product_price"));
		//String image=request.getParameter("image");
		Ecommerce eco = new Ecommerce( name,id,desc,qty,price);

		String insert="";
		try {
			int record=dao.insert(eco);
			p.print(record);
			if(record>0){
				insert="Sucessfull";
			}else {
				insert="Failed to Insert";
			}
			} catch (Exception c) {
				c.printStackTrace();
				}
			String productJsonString=new Gson().toJson(insert);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			p.print(productJsonString);
			p.flush();
	}
}
