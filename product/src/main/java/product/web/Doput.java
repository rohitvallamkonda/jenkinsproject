package product.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

//import product.com.Ecommerce;
import product.dao.EcommerceDao;

/**
 * Servlet implementation class Doput
 */
public class Doput extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Doput() {
    	
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/product");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/product");
	}
	
	/**
	 * @see HttpServlet#doPut(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3001/product");
		
		EcommerceDao dao = new EcommerceDao();
		PrintWriter out = response.getWriter();
		String status = "";

		String name = request.getParameter("product_name");
		String desc = request.getParameter("product_desc");
		int qty = Integer.parseInt(request.getParameter("product_quantity"));
		int price = Integer.parseInt(request.getParameter("product_price"));
		int id = Integer.parseInt(request.getParameter("product_id"));

		int rec = 0;
		try {
			rec = dao.update(name,id,desc,price,qty);
			if (rec > 0) {
				status = "Sucessfull";
				}
		String productJsonString = new Gson().toJson(status);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.write(productJsonString);
//		System.out.println(productJsonString);
		out.flush();
		} catch (Exception e1) {
			e1.printStackTrace();
			}
	}
}
