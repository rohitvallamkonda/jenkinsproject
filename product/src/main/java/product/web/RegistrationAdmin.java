package product.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import product.com.Admin;
import product.dao.AdminDao;

/**
 * Servlet implementation class RegistrationAdmin
 */
public class RegistrationAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		
		AdminDao dao = new AdminDao();
		PrintWriter pw = response.getWriter();
		
		String username = request.getParameter("admin_username");
		String email = request.getParameter("admin_email");
		String password = request.getParameter("admin_password");
		Admin ad = new Admin(username,email,password);
		
		try {
			int insert = dao.signup(ad);
			System.out.println(insert);
			switch(insert) {
			case 0:
	    	{
	    		pw.write("Already Registered");
	    		break;
	    		}
	    	case 1:
	    	{
	    		pw.write("Admin Registration successfull");
	    		break;
	    		}
	    	}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
